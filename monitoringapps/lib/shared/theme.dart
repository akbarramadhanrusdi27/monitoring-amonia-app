import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color whiteColor = const Color(0xffFFFFFF);
Color blackColor = const Color(0xff14193f);
Color greycolor = const Color(0xffA4A8AE);
Color lightBackgroundColor = const Color(0xffF6F8FB);
Color shapeBackgroundColor = const Color(0xffF1F1F9);
Color darkBackgroundColor = const Color(0xff020518);
Color blueColor = const Color(0xff53C1F9);
Color purpleColor = const Color(0xff5142E6);
Color greenColor = const Color(0xff22B07D);
Color numberBackgroundColo = const Color(0xff1A1D2E);
Color orangeColor = const Color(0xffFFA235);

TextStyle blackTextStyle = GoogleFonts.poppins(color: blackColor);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: whiteColor);
TextStyle greyTextStyle = GoogleFonts.poppins(color: greycolor);
TextStyle blueTextStyle = GoogleFonts.poppins(color: blueColor);
TextStyle greenTextStyle = GoogleFonts.poppins(color: greenColor);
TextStyle orangeTextStyle = GoogleFonts.poppins(color: orangeColor);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;
