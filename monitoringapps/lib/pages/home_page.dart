import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:monitoringapps/shared/theme.dart';
import 'package:monitoringapps/widgets/button.dart';
import 'package:monitoringapps/widgets/live_records_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final Uri _url = Uri.parse(
      'https://docs.google.com/spreadsheets/d/1QICDjcyn3UdGs25p6BiDM8takS43ey1kTwDDjSuy9d8/edit#gid=0');
  final Uri _urlkd = Uri.parse(
      'https://docs.google.com/spreadsheets/d/1NqDpmwcw8lN-7qO0IK9hIrcujoVcuOK4nROhCm9edvI/edit#gid=0');

  final Uri _urlkt = Uri.parse(
      'https://docs.google.com/spreadsheets/d/1ibwNzbV-hZzxMHo4hga40KV0Qa-X85z0F1V917IP4hE/edit#gid=0');

  final Uri _urlke = Uri.parse(
      'https://docs.google.com/spreadsheets/d/1MlFkW6WwB1LIwruQTtZvUQaN1ZVSJq5uNls6G9yEkeE/edit#gid=0');

  late DatabaseReference _databaseRefks;
  String amoniaValuekcs = '';

  late DatabaseReference _databaseRefkd;
  String amoniaValuekcd = '';

  late DatabaseReference _databaseRefkt;
  String amoniaValuekct = '';

  late DatabaseReference _databaseRefke;
  String amoniaVluekce = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _databaseRefks = FirebaseDatabase.instance.reference().child('/amoniakcs/');
    _databaseRefkd = FirebaseDatabase.instance.reference().child('/amoniakcd/');
    _databaseRefkt = FirebaseDatabase.instance.reference().child('/amoniakct/');
    _databaseRefke = FirebaseDatabase.instance.reference().child('/amoniakce/');

    _databaseRefks.onValue.listen((event) {
      var snapshot = event.snapshot;
      setState(() {
        amoniaValuekcs = snapshot.child('amoniak1').value.toString();
      });
    });

    _databaseRefkd.onValue.listen((event) {
      var snapshot = event.snapshot;
      setState(() {
        amoniaValuekcd = snapshot.child('amoniak2').value.toString();
      });
    });

    _databaseRefkt.onValue.listen((event) {
      var snapshot = event.snapshot;
      setState(() {
        amoniaValuekct = snapshot.child('amoniak3').value.toString();
      });
    });

    _databaseRefke.onValue.listen((event) {
      var snapshot = event.snapshot;
      setState(() {
        amoniaVluekce = snapshot.child('amoniak4').value.toString();
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _databaseRefks.onValue.listen(null);
    _databaseRefkd.onValue.listen(null);
    _databaseRefkt.onValue.listen(null);
    _databaseRefke.onValue.listen(null);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: lightBackgroundColor,
      appBar: AppBar(
        backgroundColor: purpleColor,
        centerTitle: true,
        title: Text(
          'Monitoring Amonia',
          style: whiteTextStyle.copyWith(
            fontWeight: semiBold,
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        children: [
          const SizedBox(
            height: 50,
          ),
          Text(
            'KADAR AMONIA\nKANDANG KELINCI',
            textAlign: TextAlign.center,
            style: blackTextStyle.copyWith(
              fontSize: 25,
              fontWeight: extraBold,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Column(
            children: [
              LiveRecordsWidgets(
                title: 'Kandang Kelinci 1',
                amonia: amoniaValuekcs,
                amoniaFormat: 'PPM',
                imageUrl: 'assets/revisikandang1.png',
              ),
              LiveRecordsWidgets(
                title: 'Kandang Kelinci 2',
                amonia: amoniaValuekcd,
                amoniaFormat: 'PPM',
                imageUrl: 'assets/kandang2.png',
              ),
              LiveRecordsWidgets(
                title: 'Kandang Kelinci 3',
                amonia: amoniaValuekct,
                amoniaFormat: 'PPM',
                imageUrl: 'assets/kandang3.png',
              ),
              LiveRecordsWidgets(
                title: 'Kandang Kelinci 4',
                amonia: amoniaVluekce,
                amoniaFormat: 'PPM',
                imageUrl: 'assets/kandang4.png',
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Column(
            children: [
              ButtonWidget(
                title: 'Data Amonia Kandang 1',
                onPressed: _launcherUrl,
              ),
              ButtonWidget(
                title: 'Data Amonia Kandang 2',
                onPressed: _launcherUrlkd,
              ),
              ButtonWidget(
                title: 'Data Amonia Kandang 3',
                onPressed: _launcherUrlkt,
              ),
              ButtonWidget(
                title: 'Data Amonia Kandang 4',
                onPressed: _launcherUrlke,
              ),
            ],
          ),
          const SizedBox(
            height: 100,
          ),
        ],
      ),
    );
  }

  Future<void> _launcherUrl() async {
    if (!await launchUrl(_url)) {
      throw Exception('Cloud not lunch $_url');
    }
  }

  Future<void> _launcherUrlkd() async {
    if (!await launchUrl(_urlkd)) {
      throw Exception('Cloud not lunch $_url');
    }
  }

  Future<void> _launcherUrlkt() async {
    if (!await launchUrl(_urlkt)) {
      throw Exception('Cloud not lunch $_url');
    }
  }

  Future<void> _launcherUrlke() async {
    if (!await launchUrl(_urlke)) {
      throw Exception('Cloud not lunch $_url');
    }
  }
}
