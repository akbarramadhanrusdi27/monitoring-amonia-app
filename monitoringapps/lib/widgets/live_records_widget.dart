import 'package:flutter/material.dart';
import 'package:monitoringapps/shared/theme.dart';

class LiveRecordsWidgets extends StatelessWidget {
  final String title;
  final String amonia;
  final String amoniaFormat;
  final String imageUrl;
  const LiveRecordsWidgets({
    required this.title,
    required this.amonia,
    required this.amoniaFormat,
    required this.imageUrl,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 30,
        ),
        Text(
          title,
          textAlign: TextAlign.center,
          style: blackTextStyle.copyWith(
            fontSize: 12,
            fontWeight: regular,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          height: 101,
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(imageUrl),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  amonia,
                  style: orangeTextStyle.copyWith(
                    fontSize: 29,
                    fontWeight: bold,
                  ),
                ),
                Text(
                  amoniaFormat,
                  style: whiteTextStyle.copyWith(
                    fontSize: 25,
                    fontWeight: bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
