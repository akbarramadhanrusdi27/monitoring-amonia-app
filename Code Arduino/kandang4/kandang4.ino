#include <ESP8266WiFi.h>
#include <FirebaseESP8266.h>
#include <WiFiClientSecure.h>
#include"DHT.h"
//----------------------------------------

#define DHTTYPE DHT11 

#define FIREBASE_HOST "https://amonia-usu-project-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "tSmvTJZwhr3jazathPD7RCBYgwnypPeDRXWoeX2c"

FirebaseData fbdo;


#define RL 10 //nilai RL =10 pada sensor
#define m -0.417 //hasil perhitungan gradien
#define b 0.425 //hasil perhitungan perpotongan
#define Ro 19 //hasil pengukuran RO
#define MQ_sensor A0 //definisi variabel
#define relayPin 12 // pompa

const int DHTPin = 5; 
DHT dht(DHTPin, DHTTYPE);


#define ON_Board_LED 2  

  

const char* ssid = "nisah"; 
const char* password = "belipaketlah"; 

const char* host = "script.google.com";
const int httpsPort = 443;


WiFiClientSecure client; //--> Create a WiFiClientSecure object.

String GAS_ID = "AKfycbwGh5R9rw1QYYBebAdQ8yU5UQl4uM_M6xk7w6gijnDRnF2dAII7lzBAi5-SHMvjeIos"; //--> spreadsheet script ID






void setup() {
  
  Firebase.begin(FIREBASE_HOST,FIREBASE_AUTH);
  

  dht.begin();  //--> Start reading DHT11 sensors
  delay(500);

 // put your setup code here, to run once:
  Serial.begin(115200);
  delay(500);

  dht.begin();  //--> Start reading DHT11 sensors
  delay(500);
//AMONIA//////////////////////////////////////////////

pinMode(A0,INPUT);
Serial.begin(9600);

/////////////////////////////////////////////////////////
  
  WiFi.begin(ssid, password); 
  Serial.println("");
    
  pinMode(ON_Board_LED,OUTPUT); //--> On Board LED port Direction output
  digitalWrite(ON_Board_LED, HIGH); //--> Turn off Led On Board

  pinMode(relayPin, OUTPUT);
  pinMode(A0, INPUT);

  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    //----------------------------------------Make the On Board Flashing LED on the process of connecting to the wifi router.
    digitalWrite(ON_Board_LED, LOW);
    delay(250);
    digitalWrite(ON_Board_LED, HIGH);
    delay(250);
    //----------------------------------------
  }
  //----------------------------------------
  digitalWrite(ON_Board_LED, HIGH); //--> lampu led pada node mcu menyala jika berhasil menghubungkan ke internet
  //----------------------------------------If successfully connected to the wifi router, the IP Address that will be visited is displayed in the serial monitor
  Serial.println("");
  Serial.print("Successfully connected to : ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();
  //----------------------------------------

  client.setInsecure();
}
void loop() {
/////DHT11////////

// Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  
//////////AMONIA///////////////////
float VRL; 
  float RS;  
  float ratio; 
   
  VRL = analogRead(MQ_sensor)*(5/1023.0); //konversi analog ke tegangan
   RS =(5.0 /VRL-1)*10 ;//rumus untuk RS
  ratio = RS/Ro;  // rumus mencari ratio
  float amonia1 = pow(10, ((log10(ratio)-b)/m));//rumus mencari ppm

  String Temp = "Temperature : " + String(t) + " °C";
  Serial.println(Temp);
 Serial.println(amonia1);
 sendData(amonia1,t); //--> Calls the sendData Subroutine

  if(amonia1 >= 6) {
    digitalWrite(relayPin, LOW);

    delay(2000);

    digitalWrite(relayPin, HIGH);

    delay(10000);    
    }

    if(amonia1 <=4) {
      
      digitalWrite(relayPin, HIGH);
      }

 ////KIRIM DATA FIRBASE
Firebase.setFloat(fbdo,"/amoniakce/amoniak4",amonia1);
}




//==============================================================================
//============================================================================== void sendData
// Subroutine for sending data to Google Sheets

void sendData(float am,float tem) {
  Serial.println("==========");
  Serial.print("connecting to ");
  Serial.println(host);
  
  //----------------------------------------Connect to Google host
  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    return;
  }
  //----------------------------------------

  //----------------------------------------proses pengiriman data
 String string_temperature =  String(tem);
  // String string_temperature =  String(tem, DEC); 
  String string_amonia1 =  String(am); 
  String url = "/macros/s/" + GAS_ID + "/exec?amonia1=" + string_amonia1+"&temperature="+ string_temperature;
  Serial.print("requesting URL: ");
  Serial.println(url);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
         "Host: " + host + "\r\n" +
         "User-Agent: BuildFailureDetectorESP8266\r\n" +
         "Connection: close\r\n\r\n");

  Serial.println("request sent");
  //----------------------------------------

  //----------------------------------------chek data terkirim dengan sukses atau tidak
  while (client.connected()) {
    String line = client.readStringUntil('\n');
    if (line == "\r") {
      Serial.println("headers received");
      break;
    }
  }
  String line = client.readStringUntil('\n');
  if (line.startsWith("{\"state\":\"success\"")) {
    Serial.println("esp8266/Arduino CI successfull!");
  } else {
    Serial.println("esp8266/Arduino CI has failed");
  }
  Serial.print("reply was : ");
  Serial.println(line);
  Serial.println("closing connection");
  Serial.println("==========");
  Serial.println();
  //----------------------------------------
} 
//==============================================================================
